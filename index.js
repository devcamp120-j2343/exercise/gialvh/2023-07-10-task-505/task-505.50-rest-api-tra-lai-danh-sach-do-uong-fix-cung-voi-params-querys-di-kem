//import thư viện expressjs
const express = require('express');

//khởi tạo app express
const app = express();

//khởi tạo cổng 
const port = 8000;

//khởi tạo class Drink
class Drink {
    constructor(id, drinkCode, drinkName, price, createDate, updateDate) {
        this.id = id,
            this.drinkCode = drinkCode,
            this.drinkName = drinkName,
            this.price = price,
            this.createDate = createDate,
            this.updateDate = updateDate
    }
}
const drinkClass = [
    new Drink(1, "TRATAC", "trà tắc", 10000, "14/05/2021", "14/05/2021"),
    new Drink(2, "COCA", "Cocacola", 15000, "14/05/2021", "14/05/2021"),
    new Drink(3, "PEPSI", "Pepsi", 15000, "14/05/2021", "14/05/2021")
]

app.get("/drinks-class", (req, res) => {
    
    let code = req.query.code;

    if (code === "") {
        res.json({
            drinkClass
        })
    }
    else {
        const filterDrink = drinkClass.filter(drinkClass => drinkClass.drinkCode === code);
        res.json({
            filterDrink
        })
    }

})

let drinkObj = [
    {
        id: 1,
        drinkCode: "TRATAC",
        drinkName: "Trà tắc",
        price: 10000,
        createDate: "14/05/2021",
        updateDate: "14/05/2021"
    },
    {
        id: 2,
        drinkCode: "COCA",
        drinkName: "Cocacola",
        price: 15000,
        createDate: "14/05/2021",
        updateDate: "14/05/2021"
    },
    {
        id: 3,
        drinkCode: "PEPSI",
        drinkName: "Pepsi",
        price: 15000,
        createDate: "14/05/2021",
        updateDate: "14/05/2021"
    },
]

app.get("/drinks-object", (req, res) => {

    let code = req.query.code;

    if (code === "") {
        res.json({
            drinkObj
        })
    }
    else {
        const filterDrinkObj = drinkObj.filter(drinkObj => drinkObj.drinkCode == code);
        res.json({
            filterDrinkObj            
        })
    }

})

//drink class request param drinkId
app.get("/drinks-class/:drinkId", (req, res) => {
   
    const drinkId = req.params.drinkId;

    const filterDrinkObj = drinkClass.filter(drinkClass => drinkClass.id == drinkId);
    res.json(

        filterDrinkObj
    )
})

//drink object request param drinkId

app.get("/drinks-object/:drinkId", (req, res) => {
    const drinkId = req.params.drinkId;

    const filterDrinkObj = drinkObj.filter(drinkObj => drinkObj.id == drinkId);
    res.json(
        filterDrinkObj
    )
})


app.listen(port, () => {
    console.log("App listening on port: ", port);
})